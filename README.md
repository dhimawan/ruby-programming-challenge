# programming-challenge

Blogger
================
This repository containt example blogger application which implement:

CRUD method
* Implement WYSIWYG editor with image upload capability
* List, Filter and Preview post
* You can view the live version of blogger application

[demo](http://128.199.186.140:5002/)

* username: user@example.com
* password:changeme


Author : 
Dani Himawan 


Ruby on Rails
-------------

This application requires:

- Ruby 2.2.0
- Rails 4.2.0