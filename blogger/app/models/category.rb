class Category < ActiveRecord::Base
  has_many :posts, dependent: :destroy

  def self.get_categories(params)
    self.where("name LIKE ?", "%#{params[:q]}%").order(:id).page params[:page]
  end

  def self.save_data(params)
    if params[:id].blank?
      @category = self.new params
      @category.save if @category.valid?
    else
      @category = self.find params[:id]
      @category.update(params)
    end
    @category
  end

end
