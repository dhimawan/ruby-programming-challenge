class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :category

  validates :title, presence: true, uniqueness: true
  validates :content, presence: true
  validates :category, presence: true


  def self.get_posts(params)
    self.joins(:category)
      .where("title LIKE ? OR content LIKE ? OR categories.name LIKE ?", 
        "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%")
      .order(:id)
      .page params[:page]
  end

  def self.save_post(params, user = User.new)
    # check new record
    if params[:id].blank?
      @post = self.new(params)
      @post.user = user
      @post.save
    else
      # update record
      @post = self.find(params[:id])
      @post.user = user
      @post.update(params)
    end
    @post
  end

  def self.new_stories
    new_post = self.order("id desc").limit(5)
    slide_post = self.order("id desc").limit(5).group("category_id")
    return new_post , slide_post
  end

end
