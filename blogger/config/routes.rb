Rails.application.routes.draw do
  root to: 'visitors#index'
  devise_for :users
  resources :users

  
  namespace :backend do
    resources :categories, except: :show
    resources :posts
    get '/'              => 'posts#index'
  end

  post '/tinymce_assets' => 'tinymce_assets#create'

end
